import React, { Component } from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import TvDetailLastSeason from './tv/TvDetailLastSeason';
import Moment from 'moment';
import 'moment/locale/tr';

export default class SearchItemContainer extends Component {
  render () {
    Moment.locale('tr');
      var findItems = [];
      for(let i = 0; i < this.props.items.length; i++) {
        var date = this.props.items[i].release_date ? new Date(this.props.items[i].release_date).toISOString() : '';
        findItems.push(
        <TvDetailLastSeason 
            type='search' 
            id={this.props.items[i].id}
            media_type={this.props.items[i].media_type}
            key={i}
            image={this.props.items[i].media_type === 'person' ? this.props.items[i].profile_path : this.props.items[i].poster_path}
            title={this.props.items[i].media_type === 'movie' ? this.props.items[i].title : this.props.items[i].name}
            subTitle={this.props.items[i].release_date ? Moment(date).format('MMMM DD, YYYY') : date}
            text={this.props.items[i].media_type === 'person' ? 'Kayitli ' + this.props.items[i].known_for.length + ' yapim listelendi.' : this.props.items[i].overview}>
        </TvDetailLastSeason>
        )
      }
    return (
        <ScrollView style={styles.SearchItemContainer}>
            {findItems}
        </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
    SearchItemContainer: {
      backgroundColor: '#f1f1f2',
      height: '100%',
      marginTop: 30,
      left: 0,
      paddingTop: 30,
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingHorizontal: 15
    }

})

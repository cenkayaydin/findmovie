import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableHighlight } from 'react-native';
import FilterModal from './FilterModal';

export default class Title extends Component {
  constructor(props) {
    super(props)
    this.handler = this.handler.bind(this);
    this.state = {
        opened: false
    };
  }
  handler() {
    this.setState({
        opened: !this.state.opened
    });
  }
  render () {
    if(this.props.button == 1) {
        if(this.props.icon === 'Filters') {
          var icon = require('../icons/Filters.png');
        }else if(this.props.icon === 'Arrow') {
          var icon = require('../icons/arrowPink.png');
        }else {
          var icon = require('../icons/Filters.png');
        }
        
        titleIcon = <View style={styles.titleButton}>
                        <TouchableHighlight onPress = {this.handler}>
                          <Image source={icon}></Image>
                        </TouchableHighlight>
                    </View>;
    }else {
      titleIcon = <Text> </Text>;
    }
    return (

        <View style={styles.title}>
            <Text style={this.props.style != 'weekTop' ? styles.movieTitleText : styles.titleText}>{this.props.title}</Text>
            <FilterModal action={this.handler} opened={this.state.opened}></FilterModal>
            {titleIcon}
        </View>
      

    )
  }
}

const styles = StyleSheet.create({
    title: {flex:1, justifyContent: 'space-between', flexDirection: 'row', width: '100%'},
    titleText: {color: '#373A42', fontSize: 28, fontWeight: "bold", lineHeight: 32,},
    titleButton: {width: 32, height: 32, backgroundColor: '#fff', borderRadius: 100, shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, justifyContent: 'center', alignItems: 'center'},
    movieTitleText: {
      color: '#373A42',
      fontSize: 18,
      fontWeight: 'bold',
      lineHeight: 18,
      marginBottom: 25
  },
})

import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, ScrollView } from 'react-native';
import SearchItemContainer from './SearchItemContainer';

export default class SearchView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            isLoading: true,
            searchData: []
        }
    }
    
    onFocus() {
      this.setState({
          marginTop: -150
      })
    }

    componentDidUpdate(prevProps, prevState) {
      
      if (this.state.text !== prevState.text && this.state.text !== '' && this.state.text) {
        this.fetchData(this.state.text)
      }
    }

    fetchData(text) {
      fetch('https://api.themoviedb.org/3/search/multi?api_key=b142583165dd8871eed4ef497de7c7fb&language=tr-TR&page=1&query='+ text +'&include_adult=false')
      .then((response) => response.json())
      .then((json) => {
        this.setState({ searchData: json.results });
        this.setState({ isLoading: false });
    })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
    });
    }

  render () {
    if(!this.state.isLoading && this.state.text !== '' && this.state.text) {
      var searchContainer = <SearchItemContainer items={this.state.searchData}></SearchItemContainer>
    }
    return (
        <View style={styles.searchContainer}>
          <TextInput
          style={styles.input}
          placeholder="Aradiginiz herseyi bulun..!"
          placeholderTextColor="#343a40" 
          onChangeText={(text) => this.setState({text})}
          value={this.state.text}
          ref={input => { this.textInput = input }}
          returnKeyType="search"
          onFocus={ () => this.onFocus() }
          />
          {searchContainer}
        </View>
    )
  }
}

const styles = StyleSheet.create({
      searchContainer: {
        flex: 1,
        justifyContent: 'center',
        marginBottom: 15,
      },
      searchTitle: {
        color: '#373A42',
        fontSize: 28,
        fontWeight: 'bold',
        lineHeight: 32,
        textAlign: 'center'
      },
      input: {
        height: 40,
        borderColor: '#343a40',
        borderWidth: 2,
        paddingLeft: 10,
        marginTop: 15,
        backgroundColor: '#fffff266',
        borderRadius: 8,
        color: '#343a40',
        width: '90%',
        left: 20
      },
      item: {
        padding: 10,
        fontSize: 18,
        height: 44,
      },
})

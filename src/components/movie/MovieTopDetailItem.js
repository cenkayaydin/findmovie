import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';

export default class MovieTopDetailIItem extends Component {
  render () {
      var list = []
      if(Array.isArray(this.props.text)) {
        if(this.props.text.length >= 1) {
            for(let i = 0; i < this.props.text.length; i++){
                list.push(
                    <Text key={i} style={[styles.genre, styles.topDetailItemText]}>{this.props.text[i].name},</Text>
                )
            }
        }else {
            list.push(
                <Text key={1} style={[styles.genre, styles.topDetailItemText]}>Belirtilmemis</Text>
            )
        }
      }else {
          if(this.props.text) {
            list.push(
                <Text key={1} style={styles.topDetailItemText}>{this.props.text}</Text>
            )
          }else {
            list.push(
                <Text key={1} style={[styles.genre, styles.topDetailItemText]}>Belirtilmemis</Text>
            )
          }
      }
    return (
        <View style={styles.topDetailItem}>
            <Image source={this.props.image} style={styles.topDetailItemIcon}></Image>
            {list}
        </View>
    )
  }
}

const styles = StyleSheet.create({
    topDetailItem: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    topDetailItemIcon: {
        width: 20,
        height: 20,
        marginRight: 10,
    },
    topDetailItemText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: '500'
    },
    genre: {
        marginRight: 5
    },

})

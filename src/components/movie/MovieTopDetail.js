import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import MovieTopDetailItem from './MovieTopDetailItem';
import Moment from 'moment';
import 'moment/locale/tr'

export default class MovieTopDetail extends Component {
  render () {
    Moment.locale('tr');
    var date = new Date(this.props.movie.release_date).toISOString();
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
      });
    return (
        <View style={styles.topDetail}>
                <Text style={styles.movieTitle}>{this.props.movie.title}</Text>
                <View style={styles.topDetailContent}>
                    <MovieTopDetailItem image={require('../../icons/genre.png')} text={this.props.movie.genres}></MovieTopDetailItem>
                    <MovieTopDetailItem image={require('../../icons/production.png')} 
                    text={Object.values(this.props.movie.production_companies).length >= 1 ? this.props.movie.production_companies[0].name : undefined}>
                    </MovieTopDetailItem>
                    <MovieTopDetailItem image={require('../../icons/movieLocation.png')} 
                    text={Object.values(this.props.movie.production_countries).length >= 1 ?this.props.movie.production_countries[0].name : undefined}>
                    </MovieTopDetailItem>
                    <MovieTopDetailItem image={require('../../icons/budget.png')} 
                    text={this.props.movie.budget > 0 ? formatter.format(this.props.movie.budget) : undefined}>
                    </MovieTopDetailItem>
                    <MovieTopDetailItem image={require('../../icons/date.png')} text={Moment(date).format('d MMMM YYYY')}></MovieTopDetailItem>
                </View>
            </View>
    )
  }
}

const styles = StyleSheet.create({
    topDetail: {
        backgroundColor: '#10181F',
        padding: 15,
        paddingBottom: 50,
    },
    movieTitle: {
        color: '#fff',
        fontSize: 28,
        fontWeight: 'bold',
        lineHeight: 32
    },
    topDetailContent: {
        marginTop: 10,
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },

})

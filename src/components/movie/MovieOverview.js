import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Title from '../Title';

export default class MovieOverview extends Component {
  render () {
    return (
        <View style={styles.mainDetailContent}>
            <Title title='Detay' style='movie'></Title>
            <Text style={styles.mainDetailContentText}>{this.props.overview}</Text>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    mainDetailContentText: {
        fontSize: 15,
        fontWeight: '500',
        letterSpacing: .4,
        color: '#616368',
        lineHeight: 20,
        textAlign: 'left'
    },
    mainDetailContent: {
        marginBottom: 35
    },

})

import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Title from '../Title';
import { WebView } from 'react-native-webview';

export default class MovieVideos extends Component {
    constructor() {
        super()
        this.state = {
            videos: [],
            isLoading: true,
        }
      }
      componentDidMount() {
        fetch('https://api.themoviedb.org/3/'+ this.props.mediaType +'/'+ this.props.ID +'/videos?api_key=b142583165dd8871eed4ef497de7c7fb&language=en-US')
          .then((response) => response.json())
          .then((json) => {
            this.setState({ videos: json.results});
            this.setState({ isLoading: false });
        })
          .catch((error) => console.error(error))
          .finally(() => {
            this.setState({ isLoading: false });
        });
      };
  render () {
      var iframe = null;
      if(!this.state.isLoading) {
          if(this.state.videos.length >= 1) {
            iframe = <WebView 
            style={styles.video}
            originWhitelist={['*']} 
            source={{ html: '<iframe width="1000" height="500" src="https://www.youtube.com/embed/'+this.state.videos[0].key+'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' }} />
          }else {
            iframe = <Text>Bulunamadi</Text>
          }
      }else {
        iframe = <Text>Bulunamadi</Text>
      }
    return (
        <View style={styles.MovieVideos}>
            <Title title='Fragmanlar' style='movie' button='0'></Title>
            {iframe}
        </View>
    )
  }
}

const styles = StyleSheet.create({
    MovieVideos: {
        flex: 1,
        width: '100%',
        justifyContent: 'center'
    },
    video: {
        height: 200
    }
})

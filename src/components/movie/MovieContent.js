import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Collections from '../Collections';
import MovieCast from './MovieCast';
import MovieOverview from './MovieOverview';
import MovieProviders from './MovieProviders';
import MovieVideos from './MovieVideos';

export default class MovieContent extends Component {
  render () {
    return (

        <View style={styles.mainDetail}>
            <MovieOverview overview={this.props.movie.overview}></MovieOverview>
            <MovieCast ID={this.props.movie.id} mediaType='movie'></MovieCast>
            <MovieProviders movieID={this.props.movie.id}></MovieProviders>
            <MovieVideos ID={this.props.movie.id} mediaType='movie'></MovieVideos>
            <Collections apiType='similarMovies' ID={this.props.movie.id} mediaType='movie'></Collections>
        </View>
      

    )
  }
}

const styles = StyleSheet.create({
    mainDetail: {
        backgroundColor: '#fff',
        width: '100%',
        top: -40,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        paddingVertical: 30,
        paddingHorizontal: 20
    },
})

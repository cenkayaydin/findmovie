import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import Title from '../Title';

export default class MovieProviders extends Component {
    constructor() {
        super()
        this.state = {
            providers: {},
            isLoading: true,
        }
      }
      componentDidMount() {
        fetch('https://api.themoviedb.org/3/movie/'+ this.props.movieID +'/watch/providers?api_key=b142583165dd8871eed4ef497de7c7fb&language=tr-TR')
          .then((response) => response.json())
          .then((json) => {
            this.setState({ providers: Object.values(json.results).length >= 1 ? json.results['TR'] : undefined});
            this.setState({ isLoading: false });
        })
          .catch((error) => console.error(error))
          .finally(() => {
            this.setState({ isLoading: false });
        });
      };
  render () {
    var providerList = [];
    if(!this.state.isLoading && this.state.providers !== undefined) {
        if(this.state.providers.flatrate) {
            for(let i=0; i < this.state.providers.flatrate.length; i++) {
                providerList.push(
                <Image key={i} source={{uri: 'https://image.tmdb.org/t/p/original' + this.state.providers.flatrate[i].logo_path}} style={styles.logo}></Image>
                )
            }
        }else if(this.state.providers.buy) {
            for(let i=0; i < this.state.providers.buy.length; i++) {
                providerList.push(
                <Image key={i} source={{uri: 'https://image.tmdb.org/t/p/original' + this.state.providers.buy[i].logo_path}} style={styles.logo} imageStyle={{ borderRadius: 10}}></Image>
                )
            }
        }else {
            for(let i=0; i < this.state.providers.rent.length; i++) {
                providerList.push(
                <Text key={i}>{this.state.providers.rent[i].provider_name}</Text>
                )
            }
        }  
    }else {
        providerList.push (
            <Text key={1}>Turkiye'de yayinlanmiyor.</Text>
        )
    }
    return (
        <View style={styles.Providers}>
            <Title title='Yayinlayan' style='movie'></Title>
            <View style={styles.providerContainer}>
            {providerList}
            </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    providerContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    logo: {
        width: 50,
        height: 50,
        borderRadius: 5,
        marginRight: 10
    },
    Providers: {marginBottom: 30}

})

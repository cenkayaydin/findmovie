import React, { Component } from 'react';
import { View, StyleSheet, Text, ImageBackground, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MovieContent from './MovieContent';
import MovieTopDetail from './MovieTopDetail'

export default class MovieDetail extends Component {
    constructor() {
        super()
        this.state = {
          discover: {},
          isLoading: true,
        }
      }
      componentDidMount() {
        fetch('https://api.themoviedb.org/3/movie/'+this.props.id+'?api_key=b142583165dd8871eed4ef497de7c7fb&language=tr-TR')
          .then((response) => response.json())
          .then((json) => {
            this.setState({ discover: json });
            this.setState({ isLoading: false });
        })
          .catch((error) => console.error(error))
          .finally(() => {
            this.setState({ isLoading: false });
        });
      };
    
  render () {
      if(!this.state.isLoading) {
          var movieTopDetail = <MovieTopDetail movie={this.state.discover}></MovieTopDetail>
          var movieContent = <MovieContent movie={this.state.discover}></MovieContent>
      }
    return (
        <View style={styles.MovieDetail}>
            <ImageBackground 
            source={{uri: 'https://image.tmdb.org/t/p/original' + this.state.discover.poster_path}} 
            style={styles.image} 
            resizeMode='cover'>
                <LinearGradient colors={['transparent', '#252625', '#10181F']} style={styles.child}>
                </LinearGradient>
            </ImageBackground>
            {movieTopDetail}
            {movieContent}
            
        </View>
    )
  }
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 600,
    },
    child: {
        flex: 1,
        opacity: .9
    },
})

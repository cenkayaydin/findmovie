import React, { Component } from 'react';
import { Text, View, StyleSheet, ImageBackground } from 'react-native';
import Title from '../Title';

export default class MovieCast extends Component {
    constructor() {
        super()
        this.state = {
            movieCast: [],
            isLoading: true,
        }
      }
      componentDidMount() {
        fetch('https://api.themoviedb.org/3/'+ this.props.mediaType +'/'+ this.props.ID +'/credits?api_key=b142583165dd8871eed4ef497de7c7fb&language=tr-TR')
          .then((response) => response.json())
          .then((json) => {
            this.setState({ movieCast: json.cast.slice(0,6)});
            this.setState({ isLoading: false });
        })
          .catch((error) => console.error(error))
          .finally(() => {
            this.setState({ isLoading: false });
        });
      };
  render () {
      var cast =[];
      if(!this.state.isLoading ) {
          for(let i =0; i < this.state.movieCast.length; i++) {
            cast.push(
            <View key={i} style={styles.card}>
                <ImageBackground source={{uri: 'https://image.tmdb.org/t/p/original' + this.state.movieCast[i].profile_path}} style={styles.image} imageStyle={{ borderRadius: 10}}></ImageBackground>
                <Text style={styles.personName}>{this.state.movieCast[i].name}</Text>
                <Text style={styles.personCharacter}>({this.state.movieCast[i].character})</Text>
            </View>
            )
          }
      }

    return (
        <View style={styles.mainDetailContent}>
            <Title title='Oyuncu Kadrosu' style='movie' button='1' icon='Arrow'></Title>
            <View style={styles.cardContainer}>
            {cast}
            </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    mainDetailContent: {
        marginBottom: 35
    },
    image: {
        height: 250,
        width: 175
    },
    card: {
        marginBottom: 10,
        maxWidth: '45%',
        overflow: 'hidden'
    },
    cardContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'space-between'
    },
    personName: {
        fontSize: 15,
        color: '#000',
        fontWeight: 'bold',
        width: '100%',
        textAlign: 'center',
        flexWrap: 'wrap'
    },
    personCharacter: {
        color: '#222',
        textAlign: 'center'
    }
})

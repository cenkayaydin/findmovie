import React, {Component} from 'react';
import {Text, View, StyleSheet, ImageBackground, TouchableOpacity} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Carousel from 'react-native-snap-carousel';
import Title from './Title';

export default class Collections extends Component {
    constructor() {
        super();
        this.state = {
            entries: [],
            test: null,
        };
    }

    componentDidMount() {
        var api = null;
        if (this.props.apiType === 'weekTop') {
            api = 'https://api.themoviedb.org/3/trending/movie/week?api_key=b142583165dd8871eed4ef497de7c7fb&language=tr-TR';
        } else {
            api = 'https://api.themoviedb.org/3/' + this.props.mediaType + '/' + this.props.ID + '/similar?api_key=b142583165dd8871eed4ef497de7c7fb&language=tr-TR&page=1';
        }
        fetch(api)
            .then((response) => response.json())
            .then((json) => {
                this.setState({entries: json.results});
            })
            .catch((error) => console.error(error))
            .finally(() => {
                //this.setState({ isLoading: false });
            });
    }

    _renderItem = ({item, index}) => {
        return (
            <TouchableOpacity onPress={() => {
                Actions.discover({item: item.id, mediaType: this.props.mediaType});
            }}>
                <ImageBackground source={{uri: 'https://image.tmdb.org/t/p/w500' + item.backdrop_path}}
                                 style={styles.image} imageStyle={{borderRadius: 10}}>
                    <View style={styles.darkImage}></View>
                    <Text style={styles.header}>{this.props.mediaType === 'movie' ? item.title : item.name}</Text>
                </ImageBackground>
            </TouchableOpacity>
        );
    };

    render() {
        return (

            <View style={styles.CollectionsContainer}>
                <Title style={this.props.apiType} title={this.props.apiType === 'weekTop' ? 'Populer Filmler' : 'Benzer Icerikler'}
                       button='0'></Title>
                <View style={[styles.Collections, this.props.apiType === 'weekTop' ? {marginTop: 20} : {marginTop: 0}]}>
                    <Carousel
                        layout={'default'}
                        inactiveSlideScale={1}
                        ref={(c) => {
                            this._carousel = c;
                        }}
                        data={this.state.entries}
                        renderItem={this._renderItem}
                        sliderWidth={405}
                        itemWidth={330}
                    />
                </View>
            </View>


        );
    }
}

const styles = StyleSheet.create({
    CollectionsContainer: {marginTop: 70, flex: 1, position: 'relative'},
    Collections: {left: -10},
    image: {
        height: 161,
        position: 'relative',
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.40,
        shadowRadius: 3.84,
        elevation: 2,
        padding: 20,
        width: 320,
        left: -30,
        flex: 1,
    },
    darkImage: {
        backgroundColor: '#000',
        position: 'absolute',
        left: '1%',
        bottom: 0,
        zIndex: 1,
        width: '98%',
        height: '100%',
        opacity: .1,
    },
    header: {
        color: '#fff',
        fontSize: 22,
        fontWeight: '700',
        lineHeight: 24,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.90,
        shadowRadius: 3.84,
        elevation: 2,
    },
    categoryText: {
        fontSize: 13,
        color: '#000',
        fontWeight: '700',
        shadowColor: '#010101',
        shadowOffset: {width: 0, height: 2}, shadowOpacity: 0.90, shadowRadius: 3.84, elevation: 2,
    },
    category: {
        marginTop: 5,
        flexDirection: 'row',
        width: '35%',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: '#fff',
        padding: 3,
        borderRadius: 3,
        opacity: .9,
    },
    bottomRoute: {
        position: 'absolute',
        bottom: 20,
        left: 20,
        color: '#fff',
        fontSize: 15,
        fontWeight: '700',
        shadowColor: '#000', shadowOffset: {width: 0, height: 2}, shadowOpacity: .90, shadowRadius: 3.84, elevation: 2,
    },
});

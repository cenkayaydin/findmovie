import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView,TouchableHighlight, Button } from 'react-native';
import PopularLink from './PopularLink';
import SearchView from './SearchView';

export default class PopularSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            links: [],
            isLoading: false
        }
    }
    componentDidMount() {
        fetch('https://api.themoviedb.org/3/trending/all/day?api_key=b142583165dd8871eed4ef497de7c7fb&language=tr-TR')
        .then((response) => response.json())
        .then((json) => {
          this.setState({ links: json.results.slice(0,10)});
          this.setState({ isLoading: false });
      })
        .catch((error) => console.error(error))
        .finally(() => {
          this.setState({ isLoading: false });
      });
    }
  render () {
      var linkList = [];
      if(!this.state.isLoading) {
        for(let i = 0; i < this.state.links.length; i++) {
            linkList.push (
                <PopularLink link={this.state.links[i]} key={i}></PopularLink>
            )
        }
      }
    return (
    <ScrollView style={styles.popularContainer}>
        <Text style={styles.popularTitle}>Popular</Text>
        <SearchView></SearchView>
        <ScrollView style={styles.linkSlide} horizontal={true} showsHorizontalScrollIndicator={false}>
          {linkList}
        </ScrollView>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
    popularContainer: {
        position: 'relative',
        bottom: 500,
        
      },
      popularTitle: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 28,
        lineHeight: 32,left: 20
      },
      linkSlide: {
        flexDirection: 'row',
        overflow: 'scroll',
        left: 20
      },
})
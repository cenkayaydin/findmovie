import React, { Component } from 'react';
import { Image, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux'

export default class Footer extends Component {
  render () {
    const goToHome = () => {
      Actions.home()
   }
   const goToDiscoverPage = () => {
    Actions.discover()
 }
    return (
        <View style={styles.Footer}>
          <TouchableOpacity onPress = {goToHome}>
           <Image source={require('../icons/home.png')}></Image>
          </TouchableOpacity>
          <TouchableOpacity onPress = {goToDiscoverPage}>
           <Image source={require('../icons/search.png')}></Image>
          </TouchableOpacity>
           <Image source={require('../icons/ticket.png')}></Image>
           <Image source={require('../icons/user.png')}></Image>
        </View>
      

    )
  }
}

const styles = StyleSheet.create({
    Footer: {justifyContent: 'space-around', backgroundColor: '#fff', height: 88, borderTopWidth: 1, borderTopColor: '#EEF3F5', flexDirection: 'row', paddingTop: 15}

})

import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';

export default class NotForYou extends Component {
  render () {
    return (

        <View style={styles.NotForYou}>
            <Image source={require('../icons/notForYou.png')}></Image>
            <Text style={styles.NotForYouText}>Tap to set filters.</Text>
        </View>
      

    )
  }
}

const styles = StyleSheet.create({
    NotForYou: {flexDirection: 'column', justifyContent: 'center', flex: 1, alignItems: 'center', marginTop: 10},
    NotForYouText: {fontSize: 15, color: '#D5DDDD', marginTop: 10, textAlign: 'center'}
})

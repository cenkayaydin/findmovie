import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, ImageBackground, TouchableOpacity } from 'react-native';
import Title from './Title';
import { Actions } from 'react-native-router-flux';
import Moment from 'moment';
import 'moment/locale/tr'

export default class Upcoming extends Component {
    constructor() {
        super()
        this.state = {
          entries: [],
        }
      }
    componentDidMount() {
        fetch('https://api.themoviedb.org/3/movie/upcoming?api_key=b142583165dd8871eed4ef497de7c7fb&language=en-US&page=1')
          .then((response) => response.json())
          .then((json) => {
            this.setState({ entries: json.results });
        })
          .catch((error) => console.error(error))
          .finally(() => {
            //this.setState({ isLoading: false });
        });
      }
  render () {
      var upcomingMovie = [];
      Moment.locale('tr');
      for(let i = 0; i < this.state.entries.length; i++){
          var date = new Date(this.state.entries[i].release_date).toISOString();
		upcomingMovie.push(
            <TouchableOpacity onPress = {() => {Actions.discover({item: this.state.entries[i].id, mediaType: 'movie'})}} key={i}>
                <View style={styles.Upcoming}>
                    <View style={styles.UpcomingLeft}>
                        <View style={styles.Date}>
                            <Text style={styles.DateDay}>{Moment(date).format('d')}</Text>
                            <Text style={styles.DateMonth}>{Moment(date).format('MMM')}</Text>
                        </View>
                        <View style={styles.UpcomingDots}>
                            <Image style={styles.UpcomingDots} source={require('../icons/dots.png')}></Image>
                        </View>
                    </View>
                    <View style={styles.UpcomingRight}>
                        <ImageBackground source={{uri: 'https://image.tmdb.org/t/p/w500' + this.state.entries[i].backdrop_path}} style={styles.image} imageStyle={{ borderRadius: 10}}>
                            <View style={styles.darkImage}></View>
                            <View style={styles.cardDesc}>
                                <Text style={styles.upcomingTitle}>{this.state.entries[i].title}</Text>
                            </View>
                        </ImageBackground>
                    </View>
                </View>
            </TouchableOpacity>
		)
	}
    return (

        <View style={styles.UpcomingContainer}>
            <Title title='Yakinda' button='1' icon='Arrow'></Title>
            <View style={styles.upcomingArea}>
                {upcomingMovie}
            </View>
        </View>
      

    )
  }
}

const styles = StyleSheet.create({
    upcomingArea: {marginVertical: 25},
    Upcoming: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    UpcomingLeft: {
        width: '15%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    Date: {
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {width: 0,height: 2,},
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        padding: 10,
        alignItems: 'center',
        marginBottom: 10,
        borderRadius: 10
    },
    DateDay: {
        color: '#222834',
        fontSize: 20,
        fontWeight: '500'
    },
    DateMonth: {
        color: '#C1C1C1',
        fontSize: 10,
        marginTop: 3,
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    UpcomingDots: {
        height: 150
    },
    image: {
        height: 175,
        position: 'relative',
        shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.40,shadowRadius: 3.84,elevation: 2,
        padding: 20,
        width: 300,
        flex: 1,
        position: 'relative'
      },
      cardDesc: {
          position: 'absolute',
          left: 10,
          bottom: 50
      },
      upcomingTitle: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
        lineHeight: 28,
        marginBottom: 5,
        shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.90,shadowRadius: 3.84,elevation: 2,
      },
      upcomingItems: {
          flex: 1,
          flexDirection: 'row',
          width: 150,
          justifyContent: 'space-between',
      },
      upcomingItem: {
          fontSize: 14,
          color: '#f1f1f1',
          shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.90,shadowRadius: 3.84,elevation: 2,
          fontWeight: '500'
      },
      darkImage: {
        backgroundColor: '#000',
        position: 'absolute',
        left: '1%',
        bottom: 55,
        zIndex: 1,
        width: '115%',
        height: '90%',
        opacity: .1
    },

})

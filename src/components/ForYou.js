import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import NotForYou from './NotForYou';
import Title from './Title';

export default class ForYou extends Component {
  render () {
    return (

        <View style={styles.forYou}>
            <Title title='Sana Ozel' button='1' icon='Filters'></Title>
            <NotForYou></NotForYou>
        </View>
      

    )
  }
}

const styles = StyleSheet.create({

})

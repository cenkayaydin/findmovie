import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground , ScrollView} from 'react-native';
import PopularSearch from './PopularSearch';

export default class SearchPage extends Component {
  render () {
    return (
      <View style={styles.searchPage}>
        <ImageBackground 
          source={require('../images/searchBG.png')} 
          style={styles.image} 
          resizeMode='cover'>
        </ImageBackground>
        <PopularSearch></PopularSearch>
      </View>
      

    )
  }
}

const styles = StyleSheet.create({
    searchPage: {
      },
      image: {
        height: 600,
        width: '100%',
      },
})

import React, {Component} from 'react';
import { View, StyleSheet, ScrollView} from 'react-native';
import Footer from '../Footer';
import MovieDetail from '../movie/MovieDetail';
import SearchPage from '../SearchPage';
import TvDetail from '../tv/TvDetail';

export default class DiscoverPage extends Component{
  constructor(props) {
    super(props);
    this.state = {
      item : this.props.navigation.state.params.item,
      mediaType: this.props.navigation.state.params.mediaType
    }
  }
  render () {
    if(this.state.item) {
      if(this.state.mediaType === 'movie') {
        titleIcon = <MovieDetail id={this.state.item}></MovieDetail>
      }else if(this.state.mediaType === 'tv') {
        titleIcon = <TvDetail id={this.state.item}></TvDetail>
      }
    }else {
      titleIcon = <SearchPage></SearchPage>
    }
    return (
      <View style={styles.container}>
        <ScrollView style={styles.main}>
          {titleIcon}
        </ScrollView>
        <Footer></Footer>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fcfcfc',
    flex: 1,
  }
})

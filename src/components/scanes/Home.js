import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import Collections from '../Collections';
import Discover from '../Discover';
import FilterModal from '../FilterModal';
import Footer from '../Footer';
import ForYou from '../ForYou';
import Upcoming from '../Upcoming';

const Home = () => {
  
    return (
      <View style={styles.container}>
        <ScrollView style={styles.main}>
         <ForYou></ForYou>
         <Collections apiType='weekTop' mediaType='movie'></Collections>
         <Discover></Discover>
         <Upcoming></Upcoming>
        </ScrollView>
        <Footer></Footer>
      </View>
    )
  
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fcfcfc',
    flex: 1,
  },
  main: {
    paddingHorizontal: 15,
    paddingVertical: 25,
  }
})

export default Home

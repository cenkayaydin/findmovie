import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux'

export default class PopularLink extends Component {
  render () {
    return (
        <Text 
        onPress={() => {
            Actions.discover({item: this.props.link.id, mediaType: this.props.link.media_type})
        }}
        style={styles.link}>
            {this.props.link.media_type === 'person' || this.props.link.media_type === 'tv' ? this.props.link.name : this.props.link.title}
        </Text>
    )
  }
}

const styles = StyleSheet.create({
    link: {
        color: '#fff',
        backgroundColor: '#343a40',
        opacity: .8,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
        paddingVertical: 10,
        marginRight: 20,
        borderRadius: 17,
        overflow: 'hidden'
      },

})

import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

export default class Header extends Component {
  render () {
    return (

        <View style={styles.header}>
            <Text style={styles.headerText}>{this.props.title}</Text>
        </View>
      

    )
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#fff',
    height: 80,
    alignItems: 'center',
    justifyContent: 'flex-end',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  headerText: {
      color: '#000',
      marginBottom: 10,
      fontWeight: 'bold',
      fontSize: 15
  }
})

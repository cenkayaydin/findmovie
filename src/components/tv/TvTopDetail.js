import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import MovieTopDetailItem from '../movie/MovieTopDetailItem';
import Moment from 'moment';
import 'moment/locale/tr'

export default class MovieTopDetail extends Component {
  render () {
    /*Moment.locale('tr');
    var date = new Date(this.props.tv.release_date).toISOString();
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
      });*/
    return (
        <View style={styles.topDetail}>
                <Text style={styles.movieTitle}>{this.props.tv.name}</Text>
                <View style={styles.topDetailContent}>
                    <MovieTopDetailItem image={require('../../icons/genre.png')} text={this.props.tv.genres}></MovieTopDetailItem>
                    <MovieTopDetailItem image={require('../../icons/production.png')} 
                    text={Object.values(this.props.tv.production_companies).length >= 1 ? this.props.tv.production_companies[0].name : undefined}>
                    </MovieTopDetailItem>
                    <MovieTopDetailItem image={require('../../icons/movieLocation.png')} 
                    text={Object.values(this.props.tv.production_countries).length >= 1 ?this.props.tv.production_countries[0].name : undefined}>
                    </MovieTopDetailItem>
                    <MovieTopDetailItem image={require('../../icons/status.png')} text={this.props.tv.status}></MovieTopDetailItem>
                </View>
            </View>
    )
  }
}

const styles = StyleSheet.create({
    topDetail: {
        backgroundColor: '#10181F',
        padding: 15,
        paddingBottom: 50,
    },
    movieTitle: {
        color: '#fff',
        fontSize: 28,
        fontWeight: 'bold',
        lineHeight: 32
    },
    topDetailContent: {
        marginTop: 10,
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },

})

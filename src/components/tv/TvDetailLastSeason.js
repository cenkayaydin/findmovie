import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Title from '../Title';
import { Actions } from 'react-native-router-flux';


export default class TvDetailLastSeason extends Component {
  render () {
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
      });
      if(this.props.type === 'detail') {
        var title = <Title title='Son Sezon' style='movie' button='0'></Title>
      }
    return (
        <TouchableOpacity
        onPress={() => {
            Actions.discover({item: this.props.id, mediaType: this.props.media_type})
        }}>
        <View style={styles.LastSeason}>
            {title}
            <View style={styles.lineCard}>
                <View style={styles.lineCardPoster}>
                    <Image source={this.props.image ? {uri: 'https://image.tmdb.org/t/p/w500' + this.props.image} : require('../../images/noImage.png')} style={this.props.image ? styles.image : styles.notImage} resizeMode='cover'></Image>
                </View>
                <View style={styles.lineCardContent}>
                    <Text style={styles.lineCardTitle}>{this.props.title}</Text>
                    <Text style={styles.lineCardSubTitle}>{this.props.subTitle}</Text>
                    <Text style={styles.lineCardContext} ellipsizeMode='tail' numberOfLines={4}>{this.props.text}</Text>
                </View>
            </View>
            <Text style={this.props.type === 'detail' ? styles.allSeason : styles.dNone}>Tüm Sezonları Görüntüle</Text>
        </View>
        </TouchableOpacity>
      

    )
  }
}

const styles = StyleSheet.create({
    lineCard: {
        width: '100%',
        backgroundColor: '#fff',
        flexDirection: 'row',
        borderRadius: 15,
        height: 150,
        position: 'relative',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 10,
        marginBottom: 15
    },
    lineCardPoster: {
        height: 150,
        width: 100,
        justifyContent: 'center'
    },
    image: {
        height: 150,
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 15
    },
    notImage:{
        height: 100,
        width: 100,
    },
    lineCardContent: {
        flexDirection: 'column',
        paddingHorizontal: 15,
        paddingVertical: 10,
        
        width: '80%'
    },
    lineCardTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    lineCardSubTitle: {
        fontSize: 14,
        fontWeight: '600',
        marginBottom: 5
    },
    lineCardContext: {
        width: 230,
        overflow: 'hidden',
        height: 80,
        fontSize: 13,
        lineHeight: 17,
    },
    allSeason: {
        color: '#373A42',
        fontSize: 18,
        fontWeight: 'bold',
        lineHeight: 20,
        marginBottom: 25,
        textDecorationLine: 'underline'
    },
    dNone: {
        display: 'none'
    }

})

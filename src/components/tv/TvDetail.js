import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import TvContent from './TvContent';
import TvTopDetail from './TvTopDetail'

export default class TvDetail extends Component {
    constructor() {
        super()
        this.state = {
          tvDetail: {},
          isLoading: true,
        }
      }
      componentDidMount() {
        fetch('https://api.themoviedb.org/3/tv/'+this.props.id+'?api_key=b142583165dd8871eed4ef497de7c7fb&language=tr-TR')
          .then((response) => response.json())
          .then((json) => {
            this.setState({ tvDetail: json });
            this.setState({ isLoading: false });
        })
          .catch((error) => console.error(error))
          .finally(() => {
            this.setState({ isLoading: false });
        });
      };
    
  render () {
    if(!this.state.isLoading) {
      var tvTopDetail = <TvTopDetail tv={this.state.tvDetail}></TvTopDetail>
      var tvContent = <TvContent tv={this.state.tvDetail}></TvContent>
  }
    return (
        <View style={styles.TvDetail}>
            <ImageBackground 
            source={{uri: 'https://image.tmdb.org/t/p/original' + this.state.tvDetail.poster_path}} 
            style={styles.image} 
            resizeMode='cover'>
                <LinearGradient colors={['transparent', '#252625', '#10181F']} style={styles.child}>
                </LinearGradient>
            </ImageBackground>
            {tvTopDetail}
            {tvContent}
        </View>
    )
  }
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 600,
    },
    child: {
        flex: 1,
        opacity: .9
    },
})

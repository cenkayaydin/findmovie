import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import MovieOverview from '../movie/MovieOverview';
import MovieCast from '../movie/MovieCast';
import TvDetailLastSeason from './TvDetailLastSeason';
import MovieVideos from '../movie/MovieVideos';
import Collections from '../Collections';
import Moment from 'moment';
import 'moment/locale/tr'

export default class TvContent extends Component {
  render () {
    Moment.locale('tr');
    var date = new Date(this.props.tv.last_episode_to_air.air_date).toISOString();
    return (
        <View style={styles.mainDetail}>
            <MovieOverview overview={this.props.tv.overview}></MovieOverview>
            <MovieCast ID={this.props.tv.id} mediaType='tv'></MovieCast>
              <TvDetailLastSeason
                type='detail'
                image={this.props.tv.last_episode_to_air.still_path}
                title={this.props.tv.last_episode_to_air.season_number + '.Sezon'}
                subTitle={Moment(date).format('YYYY')+ ' ' + this.props.tv.last_episode_to_air.episode_number + '.Bolum'}
                text={this.props.tv.last_episode_to_air.overview}
              ></TvDetailLastSeason>
            <MovieVideos ID={this.props.tv.id} mediaType='tv'></MovieVideos>
            <Collections apiType='similarMovies' ID={this.props.tv.id} mediaType='tv'></Collections>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    mainDetail: {
        backgroundColor: '#fff',
        width: '100%',
        top: -40,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        paddingVertical: 30,
        paddingHorizontal: 20
    },
})

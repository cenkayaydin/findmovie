import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import Title from './Title';
import Carousel from 'react-native-snap-carousel';

export default class Discover extends Component {
    constructor() {
        super()
        this.state = {
          entries: [
            { title: 'YOUR AREA', icon: require('../icons/navigationPink.png'), pillColor: '#FFC3D8', color: '#FC1055' },
            { title: 'MUSIC', icon: require('../icons/musicBlue.png'), pillColor: '#ACCCFF', color: '#5798FF' },
            { title: 'SPORTS', icon: require('../icons/goalOrenge.png'), pillColor: '#FFC8AC', color: '#E69960' },
          ],
        }
      };
      _renderItem = ({item, index}) => {
        return (
       <View style={styles.pill}>
           <View style={[styles.shadowIcon,{backgroundColor: item.pillColor}]}> 
               <Image source={item.icon}></Image>
           </View> 
           <Text style={[styles.pillText, {color: item.color}]}>{item.title}</Text> 
       </View>
       )
     };
  render () {
    return (
        <View style={styles.DiscoverContainer}>
            <Title title='Kesfet' button='0'></Title>
            <View style={styles.Discover}>
            <Carousel
                layout={'default'}
                inactiveSlideScale={1}
                ref={(c) => { this._carousel = c; }}
                data={this.state.entries}
                renderItem={this._renderItem}
                sliderWidth={600}
                itemWidth={155}
            />
            </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
    DiscoverContainer: {
        marginTop: 30, 
    },
    Discover: {
        left: -225
    },
    pill: {
        backgroundColor: '#fff', 
        borderRadius: 50, 
        padding: 10,
        shadowColor: "#ccc",
        shadowOffset: {width: 0,height: 1,},
        shadowOpacity: 0.40,
        shadowRadius: 2.84,
        elevation: 2,
        marginTop: 15,
        marginBottom: 30,
        flexDirection: 'row',
        alignItems: 'center',
        width: 145,
        height: 56
    },
    shadowIcon: {
        height: 40,
        width: 40, 
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    pillText: {
        fontSize: 12,
        color: '#FC1055',
        fontWeight: '600',
        marginLeft: 5
    }
})

import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Home from '../src/components/scanes/Home'
import DiscoverPage from './components/scanes/DiscoverPage'

const Routes = () => (
   <Router>
      <Scene key = "root">
         <Scene key = "home" component = {Home} title = "Home" initial = {true}/>
         <Scene key = "discover" component = {DiscoverPage} title = "Discover" hideNavBar={true} />
      </Scene>
   </Router>
)
export default Routes